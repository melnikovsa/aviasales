
import json
import datetime

from aiohttp import web

from settings import GRAPH
from core.helpers import default
from core.algorithms import find_flights as ff


def speed_func(x: list) -> datetime.timedelta:
    return x[-1].arrival_timestamp - x[0].departure_timestamp


def pricing_func(x: list) -> float:
    return sum([e.pricing for e in x])


FILTERS = {
    'fast': {
        'key': speed_func,
        'reverse': False
    },
    'slow': {
        'key': speed_func,
        'reverse': True
    },
    'cheap': {
        'key': pricing_func,
        'reverse': False
    },
    'expensive': {
        'key': pricing_func,
        'reverse': True
    }
}


def get_optimal(flights: list) -> list:
    min_price = min([pricing_func(f) for f in flights])
    max_price = max([pricing_func(f) for f in flights])
    min_time = min([speed_func(f).total_seconds() for f in flights])
    max_time = max([speed_func(f).total_seconds() for f in flights])
    price_int = max_price - min_price if max_price != min_price else 1
    time_int = max_time - min_time if max_time != min_time else 1

    def optimal_func(x):
        return (pricing_func(x) - min_price) / price_int * 0.5 + \
               ((x[-1].arrival_timestamp - x[0].departure_timestamp).total_seconds() -
                min_time) / time_int * 0.5

    return sorted(flights, key=optimal_func)[0]


async def find_flights(request):
    if 'source' not in request.query or not len(request.query['source']) or \
            'destination' not in request.query or not len(request.query['destination']):
        return web.HTTPBadRequest()

    try:
        flights = ff(GRAPH, request.query['source'], request.query['destination'])
    except Exception as e:
        return web.Response(body=json.dumps({'error': str(e)}), content_type='application/json')

    if 'filter' in request.query and len(str(request.query['filter']).strip()):
        f_filter = str(request.query['filter']).strip()

        if f_filter in FILTERS:
            flights = sorted(flights, key=FILTERS[f_filter]['key'], reverse=FILTERS[f_filter]['reverse'])[0]
        elif f_filter == 'optimal':
            flights = get_optimal(flights)

    elif 'difference' in request.query and len(str(request.query['difference']).strip()):
        f_difference = str(request.query['difference']).strip()
        diffs = f_difference.split(',')
        if len(diffs) != 2 or diffs[0] == diffs[1] or \
                any([d not in ('fast', 'slow', 'cheap', 'expensive', 'optimal') for d in diffs]):
            return web.HTTPBadRequest()

        results = list()
        for d in diffs:
            if d in FILTERS:
                results.append(sorted(flights, key=FILTERS[d]['key'], reverse=FILTERS[d]['reverse'])[0])
            elif d == 'optimal':
                results.append(get_optimal(flights))

        flights = {'diffs': {}}
        if len(results[0]) != len(results[1]):
            flights['diffs'].update({'transfer_count': [len(results[0]), len(results[1])]})
        if speed_func(results[0]) != speed_func(results[1]):
            flights['diffs'].update({'duration': [{
                'departure': results[0][0].departure_timestamp,
                'arrival': results[0][-1].arrival_timestamp
            }, {
                'departure': results[1][0].departure_timestamp,
                'arrival': results[1][-1].arrival_timestamp
            }]})
        if pricing_func(results[0]) != pricing_func(results[1]):
            flights['diffs'].update({'pricing': [pricing_func(results[0]), pricing_func(results[1])]})

    return web.Response(body=json.dumps(flights, default=default), content_type='application/json')
