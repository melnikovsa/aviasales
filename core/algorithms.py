
import datetime

from core.graph import Graph
from settings import TIME_BETWEEN_FLIGHTS


def find_flights(graph: Graph, source: str, destination: str) -> list:
    if source not in graph.nodes:
        raise Exception('Source not found')

    flights = list()

    def find_flight(local_destination: str, local_path: list, arrival_time: datetime) -> None:

        if local_destination == destination:
            flights.append(local_path.copy())
            return

        arrival_time_next = arrival_time + datetime.timedelta(minutes=TIME_BETWEEN_FLIGHTS)

        for flight in graph.nodes[local_destination]:
            if flight.departure_timestamp > arrival_time_next:
                local_path.append(flight)
                find_flight(flight.destination, local_path, flight.arrival_timestamp)
                local_path.pop()

    find_flight(source, list(), datetime.datetime(2010, 1, 1))

    return flights
