
import datetime
import re


def default(o):
    from core.graph import Edge
    from settings import DATETIME_FORMAT

    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.strftime(DATETIME_FORMAT)
    elif isinstance(o, Edge):
        return o.__dict__


def datetime_parser(dct):
    from settings import DATETIME_FORMAT, DATETIME_FORMAT_REGEXP

    for k, v in dct.items():
        if isinstance(v, str) and re.search(DATETIME_FORMAT_REGEXP, v):
            try:
                dct[k] = datetime.datetime.strptime(v, DATETIME_FORMAT)
            except:
                pass
    return dct
