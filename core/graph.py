import hashlib
import json

from collections import defaultdict
from datetime import datetime

from core.helpers import default


class Edge:
    __slots__ = ('carrier',
                 'flight_number',
                 'source',
                 'destination',
                 'departure_timestamp',
                 'arrival_timestamp',
                 'flight_class',
                 'number_of_stops',
                 'pricing',
                 'ticket_type')

    def __init__(self,
                 carrier: str,
                 flight_number: str,
                 source: str,
                 destination: str,
                 departure_timestamp: datetime,
                 arrival_timestamp: datetime,
                 flight_class: str,
                 number_of_stops: int,
                 pricing: float,
                 ticket_type: str) -> None:
        self.carrier = carrier
        self.flight_number = flight_number
        self.source = source
        self.destination = destination
        self.departure_timestamp = departure_timestamp
        self.arrival_timestamp = arrival_timestamp
        self.flight_class = flight_class
        self.number_of_stops = number_of_stops
        self.pricing = pricing
        self.ticket_type = ticket_type

    def __str__(self) -> str:
        return json.dumps(self.__dict__, sort_keys=True, default=default)

    @property
    def __dict__(self) -> dict:
        d = dict()
        for s in self.__slots__:
            d.update({s: self.__getattribute__(s)})

        return d

    def __eq__(self, other) -> bool:
        return all([self.__getattribute__(s) == other.__getattribute__(s) for s in self.__slots__])

    def __hash__(self):
        return hash(str(self))


class Graph:
    def __init__(self):
        """
            nodes has structure:
                {
                    'key1': [Edge1, Edge2, ..],
                    'key2': [Edge3, Edge4, ..],
                    ...
                }
            """
        self.nodes = defaultdict(set)

    def __str__(self) -> str:
        return str(self.nodes)

    def add_data(self, data: list) -> None:
        """
        Load graph nodes and edges from list
        :param data: must have structure [{
            'carrier': 'AirIndia',
            'flight_number': 996,
            'source': 'DXB',
            'destination': 'DEL',
            'departure_timestamp': '2018-10-22T0005',
            'arrival_timestamp': '2018-10-22T0445',
            'flight_class': 'G',
            'number_of_stops': 0,
            'pricing': 155.99,
            'ticket_type' : 'E'
        }, ]
        :return: None
        """
        for d in data:
            edge = Edge(d['carrier'], d['flight_number'], d['source'], d['destination'], d['departure_timestamp'],
                        d['arrival_timestamp'], d['flight_class'], d['number_of_stops'], d['pricing'], d['ticket_type'])

            self.add_edge(d['source'], edge)

    def clear(self) -> None:
        self.nodes = defaultdict(set)

    def add_edge(self, source: str, edge: Edge) -> bool:
        if edge in self.nodes[source]:
            return False

        self.nodes[source].add(edge)
        return True
