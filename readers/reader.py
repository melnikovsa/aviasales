
class Reader:
    def __init__(self, path: str) -> None:
        self.path = path

    def read(self) -> object:
        pass
