
from xml.etree import cElementTree as ElementTree
from readers.reader import Reader


class XMLReader(Reader):
    def read(self) -> object:
        tree = ElementTree.parse(self.path)
        root = tree.getroot()
        return root
