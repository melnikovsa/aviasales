
from aiohttp import web

from settings import GRAPH, AVIASALES_DATA
from views.find_flights import find_flights


def get_app():
    app = web.Application()
    app.add_routes([web.get('/find', find_flights),
                    web.get('/find/', find_flights)])
    return app


def fill_graph():
    for d in AVIASALES_DATA:
        reader = d['reader'](d['path'])
        data = d['converter']().convert(reader.read())
        GRAPH.add_data(data)


def start_app():
    fill_graph()
    web.run_app(get_app(), host='127.0.0.1', port=8000)


if __name__ == "__main__":
    start_app()
