Для установки необходимо выполнить:\
`pip3 install -r requirements.txt`


Сервис запускается на 8000 порту при помощи команды:\
`python3 app.py`


После этого можно получать данные:\
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK` - получить список всех перелетов \
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK&filter=fast` - получить самый быстрый перелет\
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK&filter=slow` - получить самый долгий перелет\
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK&filter=cheap` - получить самый дешевый перелет\
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK&filter=expensive` - получить самый дорогой перелет\
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK&filter=optimal` - получить оптимальный перелет\
GET `http://127.0.0.1:8000/find?source=DXB&destination=BKK&difference=fast,cheap` - получить разницу между самым быстрым и самым дешевым



