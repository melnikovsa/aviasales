
from converters.rs_via_xml import RSViaXMLConverter
from readers.xml_reader import XMLReader
from core.graph import Graph

AVIASALES_DATA = [{
    'path': 'data/RS_Via-3.xml',
    'reader': XMLReader,
    'converter': RSViaXMLConverter
}, {
    'path': 'data/RS_ViaOW.xml',
    'reader': XMLReader,
    'converter': RSViaXMLConverter
}]

GRAPH = Graph()

# минимальное время между посадкой и следующим вылетом (время пересадки) в минутах
TIME_BETWEEN_FLIGHTS = 30

# формат даты для преобразования в json и обратно
DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S'
DATETIME_FORMAT_REGEXP = '[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]T[0-9][0-9]:[0-9][0-9]:[0-9][0-9]'
