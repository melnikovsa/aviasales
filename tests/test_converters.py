from datetime import datetime
from unittest import TestCase

from converters.converter import Converter
from converters.rs_via_xml import RSViaXMLConverter
from readers.xml_reader import XMLReader


class RSViaXMLConverterTest(TestCase):
    def setUp(self) -> None:
        self.reader = XMLReader('tests/fixtures/RS_Via.xml')
        self.converter = RSViaXMLConverter()

    def tearDown(self) -> None:
        self.reader = None
        self.converter = None

    def test_convert_base(self):
        c = Converter()
        self.assertEqual(c.convert(object()), None)

    def test_convert(self) -> None:
        data = self.converter.convert(self.reader.read())
        self.assertEqual(len(data), 4)
        self.assertTrue(all([(x in data[0]) for x in ['carrier', 'flight_number', 'source', 'destination',
                                                      'departure_timestamp', 'arrival_timestamp', 'flight_class',
                                                      'number_of_stops', 'ticket_type', 'pricing']]))
        self.assertTrue(all([x in ['carrier', 'flight_number', 'source', 'destination',
                                   'departure_timestamp', 'arrival_timestamp', 'flight_class',
                                   'number_of_stops', 'ticket_type', 'pricing'] for x in data[0]]))

        self.assertEqual(type(data[0]['carrier']), str)
        self.assertEqual(type(data[0]['flight_number']), str)
        self.assertEqual(type(data[0]['source']), str)
        self.assertEqual(type(data[0]['destination']), str)
        self.assertEqual(type(data[0]['departure_timestamp']), datetime)
        self.assertEqual(type(data[0]['arrival_timestamp']), datetime)
        self.assertEqual(type(data[0]['flight_class']), str)
        self.assertEqual(type(data[0]['number_of_stops']), int)
        self.assertEqual(type(data[0]['ticket_type']), str)
        self.assertEqual(type(data[0]['pricing']), float)


