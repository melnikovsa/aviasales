
import json

from datetime import datetime
from unittest import TestCase

from core.graph import Edge
from core.helpers import default, datetime_parser


class HelpersTest(TestCase):
    def test_default(self) -> None:
        self.assertEqual(default(datetime(2010, 1, 1, 0, 0, 0)), '2010-01-01T00:00:00')
        e = Edge('Qatar Airways', '1003', 'DXB', 'DOH', datetime(2018, 10, 22, 6, 30, 0),
                 datetime(2018, 10, 22, 6, 40, 0), 'N', 0, 845.0, 'E')
        self.assertEqual(json.dumps(e, default=default),
                         '{"carrier": "Qatar Airways", "flight_number": "1003", "source": "DXB", "destination": "DOH", '
                         '"departure_timestamp": "2018-10-22T06:30:00", "arrival_timestamp": "2018-10-22T06:40:00", '
                         '"flight_class": "N", "number_of_stops": 0, "pricing": 845.0, "ticket_type": "E"}')
        self.assertEqual(default(123), None)

    def test_datetime_parser(self):
        f = {
            'carrier': 'Qatar Airways',
            'departure_timestamp': '2018-10-22T06:30:00',
            'arrival_timestamp': '2018-10-22T06:40:99',
            'number_of_stops': 0,
            'pricing': 845.0
        }
        self.assertEqual(datetime_parser(f)['carrier'], 'Qatar Airways')
        self.assertEqual(datetime_parser(f)['departure_timestamp'], datetime(2018, 10, 22, 6, 30, 0))
        self.assertEqual(datetime_parser(f)['arrival_timestamp'], '2018-10-22T06:40:99')
        self.assertEqual(datetime_parser(f)['number_of_stops'], 0)
        self.assertEqual(datetime_parser(f)['pricing'], 845.0)

