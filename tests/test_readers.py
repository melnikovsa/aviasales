
from unittest import TestCase

from readers.reader import Reader
from readers.xml_reader import XMLReader


class XmlReaderTest(TestCase):
    def setUp(self) -> None:
        self.reader = XMLReader('tests/fixtures/RS_Via.xml')

    def tearDown(self) -> None:
        self.reader = None

    def test_read_base(self):
        r = Reader('tests/fixtures/RS_Via.xml')
        self.assertEqual(r.read(), None)

    def test_read(self) -> None:
        root = self.reader.read()
        self.assertTrue(isinstance(root, object))
        self.assertEqual(len(root.findall('*/Flights/')), 3)
        self.assertEqual(len(root.findall('*/Flights/OnwardPricedItinerary/Flights/Flight')), 2)
