
from unittest import TestCase

from core.graph import Graph, Edge
from tests import JSONFixturesLoader


class GraphTest(TestCase, JSONFixturesLoader):
    def setUp(self) -> None:
        self.data = self._load_fixtures('tests/fixtures/flights.json')
        self.graph = Graph()

    def tearDown(self) -> None:
        self.data = None
        self.graph.clear()
        self.graph = None

    def test_add_data(self) -> None:
        self.graph.add_data(self.data)
        self.assertEqual(len(self.graph.nodes), 3)
        self.assertEqual(len(self.graph.nodes['DXB']), 2)
        self.assertEqual(len(self.graph.nodes['DEL']), 2)
        self.assertEqual(len(self.graph.nodes['BKK']), 1)

    def test_add_edge(self):
        self.graph.add_data(self.data[0:len(self.data) - 1])
        edge = Edge(**self.data[-1])
        self.assertTrue(self.graph.add_edge(self.data[-1]['source'], edge))
        self.assertFalse(self.graph.add_edge(self.data[-1]['source'], edge))

    def test_str(self):
        self.assertTrue(isinstance(str(self.graph), str))
        self.assertTrue(isinstance(str(Edge(**self.data[-1])), str))
