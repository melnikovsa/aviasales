
import json

from core.helpers import datetime_parser


class JSONFixturesLoader:
    @staticmethod
    def _load_fixtures(path) -> list:
        f = open(path, "r")
        data = f.read()
        f.close()
        return json.loads(data, object_hook=datetime_parser)
