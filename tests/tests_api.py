
from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from app import get_app, fill_graph
from settings import GRAPH


class AppTestCase(AioHTTPTestCase):
    async def setUpAsync(self) -> None:
        fill_graph()

    async def tearDownAsync(self) -> None:
        GRAPH.clear()

    async def get_application(self):
        """
        Override the get_app method to return your application.
        """
        return get_app()

    # the unittest_run_loop decorator can be used in tandem with
    # the AioHTTPTestCase to simplify running
    # tests that are asynchronous
    @unittest_run_loop
    async def test_find_all(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

        resp = await self.client.request("GET", "/find")
        assert resp.status == 400

    @unittest_run_loop
    async def test_find_fast(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&filter=fast")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

    @unittest_run_loop
    async def test_find_slow(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&filter=slow")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

    @unittest_run_loop
    async def test_find_cheap(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&filter=cheap")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

    @unittest_run_loop
    async def test_find_expensive(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&filter=expensive")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

    @unittest_run_loop
    async def test_find_optimal(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&filter=optimal")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

    @unittest_run_loop
    async def test_find_difference(self):
        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&difference=cheap,optimal")
        assert resp.status == 200
        data = await resp.json()
        assert len(data)

        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&difference=cheap")
        assert resp.status == 400

        resp = await self.client.request("GET", "/find?source=DXB&destination=BKK&difference=cheap,quick")
        assert resp.status == 400

    @unittest_run_loop
    async def test_find_errors(self):
        resp = await self.client.request("GET", "/find?source=DXB1&destination=BKK&difference=cheap,optimal")
        assert resp.status == 200
        data = await resp.json()
        assert data.get('error', '') == 'Source not found'
