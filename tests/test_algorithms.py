
from unittest import TestCase

from core.algorithms import find_flights
from core.graph import Graph
from tests import JSONFixturesLoader


class AlgorithmTest(TestCase, JSONFixturesLoader):
    def setUp(self) -> None:
        self.data = self._load_fixtures('tests/fixtures/flights.json')
        self.graph = Graph()
        self.graph.add_data(self.data)

    def tearDown(self) -> None:
        self.data = None
        self.graph.clear()
        self.graph = None

    def test_find_flights(self) -> None:
        try:
            find_flights(self.graph, 'DXB1', 'BKK')
        except Exception as e:
            self.assertEqual(str(e), 'Source not found')

        flights = find_flights(self.graph, 'DXB', 'BKK')
        self.assertEqual(len(flights), 1)
        self.assertEqual(len(flights[0]), 2)

        flights = find_flights(self.graph, 'DEL', 'DXB')
        self.assertEqual(len(flights), 2)
        if len(flights[0]) == 3:
            self.assertEqual(len(flights[0]), 3)
            self.assertEqual(len(flights[1]), 1)
        else:
            self.assertEqual(len(flights[0]), 1)
            self.assertEqual(len(flights[1]), 3)

        flights = find_flights(self.graph, 'DEL', 'DOH')
        self.assertFalse(len(flights))

        flights = find_flights(self.graph, 'DOH', 'DXB')
        self.assertFalse(len(flights))


