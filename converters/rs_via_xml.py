
from datetime import datetime

from converters.converter import Converter


class RSViaXMLConverter(Converter):

    DATETIME_FORMAT = '%Y-%m-%dT%H%M'

    def convert(self, data: object) -> list:
        root = data

        result = list()
        for f in root.findall("./PricedItineraries/Flights"):
            pricing = \
                f.findall("Pricing[@currency='SGD']/ServiceCharges[@type='SingleAdult'][@ChargeType='TotalAmount']")
            if len(pricing) > 1:
                raise Exception('Few prices')

            pricing = float(pricing[0].text)

            for f1 in f.findall("*/Flights/Flight"):
                departure_timestamp = datetime.strptime(f1.find("DepartureTimeStamp").text, self.DATETIME_FORMAT)
                arrival_timestamp = datetime.strptime(f1.find("ArrivalTimeStamp").text, self.DATETIME_FORMAT)
                result.append({
                    'carrier': f1.find("Carrier").text,
                    'flight_number': f1.find("FlightNumber").text,
                    'source': f1.find("Source").text,
                    'destination': f1.find("Destination").text,
                    'departure_timestamp': departure_timestamp,
                    'arrival_timestamp': arrival_timestamp,
                    'flight_class': f1.find("Class").text,
                    'number_of_stops': int(f1.find("NumberOfStops").text),
                    'ticket_type': f1.find("TicketType").text,
                    'pricing': pricing
                })

        return result
